# KeyValue


## Description

A key value store where you can add or delete items using commands like `put`, `pop` and also can view the keys and values using the `get(:keys)` or `get(:values)` command. We can also stop the genserver by running 

``` 
KeyValue.Stop()
```

## Testing
Testing the unit test cases by running

```
mix test
```

