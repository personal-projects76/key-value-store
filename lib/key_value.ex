defmodule KeyValue do
  use GenServer

  @name KV
  ## Client
  # here the genserver gets started and its acting like a process
  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts ++ [name: KV])
  end

  def put(key, value) do
    GenServer.cast(@name, {:put, key, value})
  end

  def pop(key) do
    GenServer.cast(@name, {:pop, key})
  end

  def get(:keys) do
    GenServer.call(@name, {:get_keys})
  end

  def get(:values) do
    GenServer.call(@name, {:get_values})
  end

  def stop do
    GenServer.stop(@name)
  end

  ## Server
  def init(:ok) do
    {:ok, %{}}
  end

  def handle_cast({:put, key, value}, state) do
    new_state = Map.put(state, key, value)
    {:noreply, new_state}
  end

  def handle_cast({:pop, key}, state) do
    new_state = Map.delete(state, key)
    {:noreply, new_state}
  end

  def handle_call({:get_keys}, _from, state) do
    keys = Map.keys(state)
    {:reply, keys, state}
  end

  def handle_call({:get_values}, _from, state) do
    values = Map.values(state)
    {:reply, values, state}
  end
end
