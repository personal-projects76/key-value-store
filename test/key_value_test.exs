defmodule KeyValueTest do
  use ExUnit.Case
  doctest KeyValue

  test "starting our genserver and looking at the initial state" do
    KeyValue.start_link()
    assert KeyValue.get(:keys) == []
    assert KeyValue.get(:values) == []
  end

  test "putting values in key value store and verifying the state" do
    KeyValue.start_link()

    KeyValue.put("name", "Owais")
    assert KeyValue.get(:keys) == ["name"]
    assert KeyValue.get(:values) == ["Owais"]
  end

  test "deleting keys from key value store and verifying the state" do
    KeyValue.start_link()

    KeyValue.put("name", "Owais")
    KeyValue.pop("name")

    assert KeyValue.get(:keys) == []
    assert KeyValue.get(:values) == []
  end
end
